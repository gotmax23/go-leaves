# go-leaves

[Changes/Mass_Retire_Golang_Leaves] management.

[Changes/Mass_Retire_Golang_Leaves]: https://fedoraproject.org/wiki/Changes/Mass_Retire_Golang_Leaves

https://pagure.io/GoSIG/go-leaves/blob/main/f/leaves ([by maintainer]) contains
the current list of leaves.

## Refresh data

This is refreshed automatically on a nightly basis.
See https://git.sr.ht/~gotmax23/fedora-scripts/tree/main/item/.builds/main.yml.

```
pip install fedrq
```

or

```
sudo dnf copr enable -y gotmax23/fedrq
sudo dnf install -y fedrq
```

and

```
curl -O https://git.sr.ht/~gotmax23/fedora-scripts/blob/main/go-sig/go_leaves.py
python3 go_leaves.py all -I $(ls opt-out/)
curl -O https://pagure.io/fedora-misc-package-utilities/raw/master/f/find-package-maintainers
for type in "all_packages" "binaries" "leaves" "outliers"; do
    python3 find-package-maintainers <"go-leaves/${type}" >"go-leaves/${type}-by-maintainer"
done
grep ' orphan$' go-leaves/outliers-by-maintainer | cut -f1 -d' ' >go-leaves/outliers-orphan
```

or

```
for package in $(ls opt-out/); do
    sed -i "/${package}/d" leaves
done
curl -O https://git.sr.ht/~gotmax23/fedora-scripts/blob/main/go-sig/go_leaves.py
python3 go_leaves.py binaries -o binaries
curl -O https://pagure.io/fedora-misc-package-utilities/raw/master/f/find-package-maintainers
for type in "all_packages" "binaries" "leaves" "outliers"; do
    python3 find-package-maintainers <"go-leaves/${type}" >"go-leaves/${type}-by-maintainer"
done
grep ' orphan$' go-leaves/outliers-by-maintainer | cut -f1 -d' ' >go-leaves/outliers-orphan
```

for a frozen refresh.

## Opting out

If your package is in the
[leaves list](https://pagure.io/GoSIG/go-leaves/blob/main/f/leaves)
([by maintainer]), you may opt out of the automatic retirement.
For instance, one of the "leaves" may be a dependency for a new piece of
software you're packaging.

To opt out, simply create a text file `opt-out/{component}`
and commit it to this repository.
Please include a brief justification.
Those without commit access may submit a PR.

```
echo "Dependency for foo (review bug #XXXXX)" > ./opt-out/bar
git add ./opt-out/bar
git commit -m "opt out bar"
git push origin
```

[by maintainer]: https://pagure.io/GoSIG/go-leaves/blob/main/f/leaves-by-maintainer
